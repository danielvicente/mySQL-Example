const db = require("../config/db");

class Post {
    constructor(title, body) {
        this.title = title;
        this.body = body;
    }

    async save() {
        let d = new Date(); //Get the Current Date
        let yyyy = d.getFullYear(); // Extract year
        let mm = d.getMonth() + 1; // Month in JavaScript is Zero index
        let dd = d.getDate();

        let createdAtDate = `${yyyy}-${mm}-${dd}`;

        //Query for the Database, should be equal to your table
        let sql = `
        INSERT INTO posts(
            title,
            body,
            created_at
        )
        VALUES(
            '${this.title}',
            '${this.body}',
            '${createdAtDate}'
        )
        `; //For SQL use '' always in strings

        const [newPost, _] = await db.execute(sql);

        return newPost;
    }

    static findAll() {
        // A static field allows this function to be accessed 
        // without creating a new object
        let sql = "SELECT * FROM posts;";
        return db.execute(sql) 
        // here we dont use await because we can just return the promise

    }

    static findById(id) {
        let sql = `SELECT * FROM posts WHERE id = ${id};`;

        return db.execute(sql);
    }
}

module.exports = Post;