// Config our Database and Connect
const mysql = require('mysql2'); // Using the mysql2 package

const pool = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_NAME,
    database: process.env.DB_PASSWORD
});
// export it assynchronously as a promise
module.exports = pool.promise();