# MySQL Example

Example following the YT video:

https://www.youtube.com/watch?v=344Zv2m9TYI

This is a simple Blog Database example in NodeJS interacting with MySQL database.

## Modules

```
  "dependencies": {
    "dotenv": "^16.0.1",
    "express": "^4.18.1",
    "mysql2": "^2.3.3",
    "nodemon": "^2.0.16"
  }
```

## Run NodeJS using npm

```
git clone https://gitlab.com/danielvicente/mySQL-Example.git mySQL-Example
npm install  # install the dependencies
npm run dev  # run and updates itself if you edit and save the project
```

Note that this is just a proposal of architecture.