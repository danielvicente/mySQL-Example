const Post = require('../models/Post');

// Asynchronous functions because we are talking with a database
exports.getAllPosts = async (req, res, next) => {
    res.send("Get all posts route");
    try {
        const posts = await Post.findAll();

        res.status(200).json(posts);
    } catch (error) {
        console.log(error);
        next(error); // send it to our global error handler
    }
}

exports.createNewPost = async (req, res, next) => {
    try {
        // Extract Json body info
        // express module takes care of that
        let { title, body } = req.body;

        //Create our Post to the DB
        let post = new Post(title, body);
        // let post = new Post("First Post", "Body of first post");

        post = post.save();

        res.status(200).json({ message: "Post Created" });

    } catch (error) {
        console.log(error);
        next(error);
    }
}

exports.getPostById = async (req, res, next) => {
    try {
        let postId = req.params.id;
        let [post, _] = await Post.findById(postId);

        res.status(200).json({post: post[0]});
    } catch (error) {
        console.log(error);
        next(error);
    }
}