const express = require('express');
const app = express();

//Middleware
app.use(express.json()); // parse json bodies in the request object

// Redirect requests to endpoint starting with /post to postRoutes.js
app.use("/posts", require("./routes/postRoutes"));

app.use("/", (req, res, next) => {
    res.status(200).json({
        message: 'MySQL nodejs example'
    });
});

// Error Handling - 404
app.use((req, res, next) => {
    const error = new Error("Not Found");
    error.status = 404;
    next(error);
});

// Global Error Handler
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message,
        },
    });
});

module.exports = app; 