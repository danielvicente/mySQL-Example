require("dotenv").config(); // ALLOWS ENV. VAR. TO BE SET AT PROCESS.ENV, SHOULD BE AT TOP
const http = require('http');

const HOST = '0.0.0.0';
const PORT = process.env.PORT || 3000;

const app = require('./app');

const server = http.createServer(app);

server.listen(PORT,HOST , () => {
        console.info(`Listening at Address: ${HOST}:${PORT}`);
});
